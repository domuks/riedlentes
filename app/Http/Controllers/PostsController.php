<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Post;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(10);

        return view('posts.index', compact('posts'));
    }


    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:255',
            'category'       => 'required|max:255',
            'description'       => 'required',
        ]);

        Post::create([
            'title'     => $request->input('title'),
            'category' => $request->input('category'),
            'description' => $request->input('description'),
            'user_id'   => Auth::user()->id,
        ]);

        return redirect('/admin');
    }

    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }


    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title'       => 'required|max:255',
            'category'       => 'required|max:255',
            'description'       => 'required',
        ]);

        $post->update($request->all());
        return back();
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return back();
    }

    public function category($id)
    {

        $posts = Post::where('category', $id)->paginate(10);

        return view('posts.index', compact('posts'));
    }

}
