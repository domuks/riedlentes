<div class="jumbotron">
    <div class="container">
        <h1 class="text-center">Jus sveikina riedlenčių pasaulis!</h1>
        <p class="text-center">Vokiška kokybė už kinietišką kainą</p>
        @guest
        <p class="text-center"><a class="btn btn-primary btn-lg" href="{{ route('register') }}" role="button">Registracija</a>
            <a class="btn btn-primary btn-lg" href="{{ route('login') }}" role="button">Prisijungti</a></p>
        @else
        <p class="text-center"><a class="btn btn-primary btn-lg" href="{{ url('/posts/create') }}" role="button">Nauja prekė</a></p>
        @endguest
    </div>
</div>