@extends('layouts.main')


@section('content')

    <h2>{{ $post->title }}</h2>
    <p>{{ $post->description }}</p>

    @if(Auth::id() == $post->user_id)
        <form action="/posts/{{ $post->id }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-flat btn-danger">Ištrinti</button>
            <input type="button" class="btn btn-flat btn-success" value="Redaguoti" onclick="window.location.href='/posts/{{$post->id}}/edit'" />
        </form>
    @endif

    <div class="comments">
        <h4>Komentarai</h4>
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li class="list-group-item">{{$comment->created_at->format('y m, d H:m') }}<strong>{{$comment->user_name}}: </strong>{{$comment->body}}</li>
                    <p>IP:{{$comment->ip_address}}</p>
                @endforeach
            </ul>
    </div>

    <hr>

    <div class="form-group">
            <form method="POST" action="/posts/{{$post->id}}/comments">
                {{csrf_field()}}
                @if (Auth::user())
                <label class="label">Komentaras: </label>
                <div class="col-md-10">
                    <input name="user_name" type="hidden" value="{{ Auth::user()->name }}">
                    <textarea type="text" class="form-control" name="body" placeholder="Kiets komentaras" ></textarea>
                </div>
                @else
                    <label class="label">Vardas: </label>
                    <div class="col-md-10">
                    <input type="text" name="user_name" class="form-control" placeholder="Kiets vardas">
                    </div>

                    <label class="label">Komentaras: </label>
                    <div class="col-md-10">
                        <textarea type="text" class="form-control" name="body" placeholder="Kiets komentaras" ></textarea>
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Paskelbti komentarą
                    </button>
                </div>
            </form>
    </div>
@endsection
